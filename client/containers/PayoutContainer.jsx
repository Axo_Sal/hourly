import Payout from '../components/Payout.jsx'

var PayoutContainer = React.createClass({

    getInitialState: function () {
        return {
            payoutValue: ''
        };
    },

    payout: function (e) {

        var payoutVal = parseInt(this.state.payoutValue);

        if (payoutVal) {

            var idsToRemove = [];

            for (var i in Earnings.find().fetch()) {

                var emv = parseInt(Earnings.find().fetch()[i].money);
                var id = Earnings.find().fetch()[i]._id;

                if (payoutVal >= emv) {
                    payoutVal -= emv;
                    idsToRemove.push(id);
                    this.setState({ payoutValue: "" });
                } else {
                    emv -= payoutVal;
                    Meteor.call("earningsUpdate", id, emv);
                    document.getElementById(id).value = emv;
                    this.setState({ payoutValue: "" });
                    break;
                }

            }

            for (var a in idsToRemove) {
                Meteor.call("earningsRemove", idsToRemove[a]);
            }

            idsToRemove = [];

        }

    },

    payoutValue: function (e) {
        this.setState({ payoutValue: e.target.value });
    },

    render: function () {
        return (
            <Payout
                payout={this.payout}
                payoutValue={this.state.payoutValue}
                payoutValueFunc={this.payoutValue} />
        );
    }
});

module.exports = PayoutContainer;