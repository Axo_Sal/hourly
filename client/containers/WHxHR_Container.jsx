import WHxHR from '../components/WHxHR.jsx'

var WHxHR_Container = React.createClass({

    workedHours: function (e) {
        var whValue = e.target.value;

        if (WorkedHours.find().fetch()[0]) {
            Meteor.call("workedHoursUpdate", WorkedHours.find().fetch()[0]._id, whValue);
        } else {
            Meteor.call("workedHoursInsert", whValue);
        }

    },

    hourlyRate: function (e) {
        var hrValue = e.target.value;

        var hourlyRateData = HourlyRate.find().fetch()[0];

        if (hourlyRateData) {
            Meteor.call("hourlyRateUpdate", hourlyRateData._id, hrValue);
        } else {
            Meteor.call("hourlyRateInsert", hrValue);
        }

    },

    whXhr: function () {

        var wh = this.props.wh;
        var hr = this.props.hr;
        var whXhr;

        if (wh && hr) {
            whXhr = wh * hr;
            Meteor.call("earningsInsert", whXhr);
        }

    },

    render: function () {
        return (
            <WHxHR
                wh={this.props.wh}
                workedHours={this.workedHours}
                hr={this.props.hr}
                hourlyRate={this.hourlyRate}
                whXhr={this.whXhr} />
        );
    }
});

module.exports = WHxHR_Container;