import ExistingEarnings from '../components/ExistingEarnings.jsx'

var ExistingEarningsContainer = React.createClass({

    getInitialState: function () {
        return {
            value: ''
        };
    },

    onAdd: function (event) {
        var value = this.state.value;
        if (value) {
            Meteor.call("earningsInsert", value);
            this.setState({ value: "" });
        }
    },

    handleChange: function (event) {
        this.setState({ value: event.target.value });
    },

    render: function () {
        return (
            <ExistingEarnings
                onAdd={this.onAdd}
                value={this.state.value}
                handleChange={this.handleChange} />
        );
    }
});

module.exports = ExistingEarningsContainer;