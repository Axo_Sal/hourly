import StartEnd from '../components/StartEnd.jsx'

var StartEndContainer = React.createClass({

    startChange: function (e) {
        var startTimeValue = e.target.value;

        if (StartTime.find().fetch()[0]) {
            Meteor.call("startTimeUpdate", StartTime.find().fetch()[0]._id, startTimeValue);
        } else {
            Meteor.call("startTimeInsert", startTimeValue);
        }

    },

    endChange: function (e) {
        var endTimeValue = e.target.value;

        if (EndTime.find().fetch()[0]) {
            Meteor.call("endTimeUpdate", EndTime.find().fetch()[0]._id, endTimeValue);
        } else {
            Meteor.call("endTimeInsert", endTimeValue);
        }

    },

    hourlyRate: function (e) {
        var hrValue = e.target.value;

        var hourlyRateData = HourlyRate.find().fetch()[0];

        if (hourlyRateData) {
            Meteor.call("hourlyRateUpdate", hourlyRateData._id, hrValue);
        } else {
            Meteor.call("hourlyRateInsert", hrValue);
        }

    },

    breaks: function (e) {
        var breaks = e.target.value;

        if (Breaks.find().fetch()[0]) {
            Meteor.call("breaksUpdate", Breaks.find().fetch()[0]._id, breaks);
        } else {
            Meteor.call("breaksInsert", breaks);
        }

    },

    startEndBtn: function () {
        var start = this.props.start;
        var end = this.props.end;
        // console.log(start); format ex. = 2016-01-31T13:00 general = yyyy-mm-ddThh:mm
        // console.log(end);
        var hr = this.props.hr;
        var diffInHours = (new Date(end) - new Date(start)) / 36e5; // 36e5 is the scientific notation for (1000*60*60)
        var breaks = this.props.breaks;
        var result;
        if (breaks) {
            result = (diffInHours - (breaks / 60)) * hr;
            Meteor.call("earningsInsert", result);
        } else {
            result = diffInHours * hr;
            Meteor.call("earningsInsert", result);
        }

    },

    render: function () {
        return (
            <StartEnd
                datetimeLocal= {this.props.datetimeLocal}
                startChange = {this.startChange}
                datetime = {this.props.datetime}
                start = {this.props.start}
                endChange = {this.endChange}
                end = {this.props.end}
                hr = {this.props.hr}
                hourlyRate = {this.hourlyRate}
                breaks = {this.props.breaks}
                breaksFunc = {this.breaks}
                startEndBtn = {this.startEndBtn} />
        );
    }
});

module.exports = StartEndContainer;