injectTapEventPlugin();

var {
    RaisedButton,
    TextField
} = MUI;

function Payout(props) {
    var payout = props.payout;
    var payoutValue = props.payoutValue;
    var payoutValueFunc = props.payoutValueFunc;
    return (
        <div>
            <TextField
                onEnterKeyDown={payout}
                value={payoutValue}
                onChange={payoutValueFunc}
                style={{ width: '150px' }}
                className="nif"
                hintText="Type in your payout"
                type="number" />

            <RaisedButton onClick={payout} label="Payout" secondary={true} />
        </div>
    );
}

Payout.propTypes = {
    payout: React.PropTypes.func.isRequired,
    payoutValue: React.PropTypes.string.isRequired,
    payoutValueFunc: React.PropTypes.func.isRequired
}

module.exports = Payout;