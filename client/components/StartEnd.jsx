injectTapEventPlugin();

var {
    RaisedButton,
    TextField
} = MUI;

function StartEnd(props) {
    var datetimeLocal = props.datetimeLocal;
    var startChange = props.startChange;
    var datetime = props.datetime;
    var start = props.start;
    var endChange = props.endChange;
    var end = props.end;
    var hr = props.hr;
    var hourlyRate = props.hourlyRate;
    var breaks = props.breaks;
    var breaksFunc = props.breaksFunc;
    var startEndBtn = props.startEndBtn;
    return (
        <div>
            <div className="startedAt">
                <p>Started working at:</p>
                <p>{datetimeLocal}</p>
                <TextField
                    onChange={startChange}
                    type="datetime-local"
                    placeholder={datetime}
                    defaultValue={start}
                />
            </div>

            <div className="endedAt">
                <p>And ended at:</p>
                <TextField
                    onChange={endChange}
                    type="datetime-local"
                    placeholder={datetime}
                    defaultValue={end}
                />
            </div>

            <div>
                <TextField
                    id="hr2"
                    value={hr}
                    onChange={hourlyRate}
                    style={{ width: '80px' }}
                    className="hourlyRate"
                    hintText="Hourly rate"
                    type="number" />

                <TextField
                    defaultValue={breaks}
                    onChange={breaksFunc}
                    className="hourlyRate"
                    hintText="Breaks, if you had any. In minutes"
                    type="number" />

                <RaisedButton label="Add" onClick={startEndBtn} secondary={true} />
            </div>
        </div>
    );
}

StartEnd.propTypes = {
    datetimeLocal: React.PropTypes.string.isRequired,
    startChange: React.PropTypes.func.isRequired,
    datetime: React.PropTypes.string.isRequired,
    start: React.PropTypes.string.isRequired,
    endChange: React.PropTypes.func.isRequired,
    end: React.PropTypes.string.isRequired,
    hr: React.PropTypes.string.isRequired,
    hourlyRate: React.PropTypes.func.isRequired,
    breaks: React.PropTypes.string.isRequired,
    breaksFunc: React.PropTypes.func.isRequired,
    startEndBtn: React.PropTypes.func.isRequired
}

module.exports = StartEnd;