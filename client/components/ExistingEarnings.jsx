injectTapEventPlugin();

var {
    RaisedButton,
    TextField
    } = MUI;

function ExistingEarnings(props) {
    var onAdd = props.onAdd;
    var value = props.value;
    var handleChange = props.handleChange;
    return (
        <div>
            <TextField
                onEnterKeyDown={onAdd}
                id="earningsInput"
                value={value}
                onChange={handleChange}
                hintText="Type in earnings you already have"
                type="number" />
            &nbsp;
            <RaisedButton label="Add" secondary={true} onClick={onAdd} />
        </div>
    );
}

ExistingEarnings.propTypes = {
    onAdd: React.PropTypes.func.isRequired,
    value: React.PropTypes.string.isRequired,
    handleChange: React.PropTypes.func
}

module.exports = ExistingEarnings;