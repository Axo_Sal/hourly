injectTapEventPlugin();

var {
    RaisedButton,
    TextField
} = MUI;

function WHxHR(props) {
    var wh = props.wh;
    var workedHours = props.workedHours;
    var hr = props.hr;
    var hourlyRate = props.hourlyRate;
    var whXhr = props.whXhr;
    return (
        <div>
            <TextField
                id="wh"
                defaultValue={wh}
                onChange={workedHours}
                style={{ width: '100px' }}
                hintText="Worked hours"
                type="number" />
            <p className="times inline-block">X</p>
            <TextField
                id="hr1"
                value={hr}
                onChange={hourlyRate}
                style={{ width: '80px' }}
                className="hourlyRate"
                hintText="Hourly rate"
                type="number" />
            <RaisedButton label="Add" onClick={whXhr} secondary={true} />
        </div>
    );
}

WHxHR.propTypes = {
    wh: React.PropTypes.string.isRequired,
    workedHours: React.PropTypes.func.isRequired,
    hr: React.PropTypes.string.isRequired,
    hourlyRate: React.PropTypes.func.isRequired,
    whXhr: React.PropTypes.func.isRequired
}

module.exports = WHxHR;