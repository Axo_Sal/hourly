Meteor.subscribe("workedHours");
Meteor.subscribe("hourlyRate");
Meteor.subscribe("startTime");
Meteor.subscribe("endTime");
Meteor.subscribe("breaks");
Meteor.subscribe("earnings");

import ExistingEarningsContainer from './containers/ExistingEarningsContainer.jsx'
import PayoutContainer from './containers/PayoutContainer.jsx'
import WHxHR_Container from './containers/WHxHR_Container.jsx'
import StartEndContainer from './containers/StartEndContainer.jsx'

injectTapEventPlugin();

var {
  RaisedButton,
  TextField
} = MUI;

App = React.createClass({
  // This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  // Loads items from the Earnings collection and puts them into this.data.earnings. Meteor magic.
  getMeteorData: function () {

    var earningsData = [];
    var total = 0;

    for (var i in Earnings.find().fetch()) {
      earningsData.push(
        <li key={Earnings.find().fetch()[i]._id}>

          <TextField
            style={{ width: '115px' }}
            defaultValue={Earnings.find().fetch()[i].money}
            className="edit"
            id={Earnings.find().fetch()[i]._id}
            onChange={this.changeInput}
            hintText="Change if you typed in wrong"
            type="number" />

          {/*<TextField
            hintText="Notes" />*/}

          <RaisedButton
            style={{ marginLeft: '5px' }}
            backgroundColor="#F44336"
            onClick={this.delete.bind(this, Earnings.find().fetch()[i]._id)}
            label="Delete" primary={true} />

        </li>
      );
      total = total + parseInt(Earnings.find().fetch()[i].money);
    }

    var crd = new Date();

    var YYYY = crd.getFullYear();
    var MM = (crd.getMonth() + 1);
    var DD = crd.getDate();
    var HH = crd.getHours();
    var mm = crd.getMinutes();

    if (MM < 10) {
      MM = "0" + MM;
    }

    if (DD < 10) {
      DD = "0" + DD;
    }

    if (HH < 10) {
      HH = "0" + HH;
    }

    if (mm < 10) {
      mm = "0" + mm;
    }

    var datetime = YYYY + "-" + MM + "-" + DD + "T" + HH + ":" + mm;

    var datetimeLocal;

    if (!Modernizr.inputtypes['datetime-local']) {
      datetimeLocal = "Date format is: yyyy-MM-ddThh:mm. Time format is 24h. Ex. " + datetime;
    }
    else {
      // console.log("datetime-local is NOT supported");
      // build one yourself
      datetimeLocal = "";
    }

    var workedHours;
    if (WorkedHours.find().fetch()[0]) {
      workedHours = WorkedHours.find().fetch()[0].workedHours;
    } else {
      workedHours = "";
    }

    var hourlyRate;
    if (HourlyRate.find().fetch()[0]) {
      hourlyRate = HourlyRate.find().fetch()[0].hourlyRate;
    } else {
      hourlyRate = "";
    }

    var startTime;
    if (StartTime.find().fetch()[0]) {
      startTime = StartTime.find().fetch()[0].startTime;
    } else {
      startTime = datetime;
    }

    var endTime;
    if (EndTime.find().fetch()[0]) {
      endTime = EndTime.find().fetch()[0].endTime;
    } else {
      endTime = datetime;
    }

    var breaks;
    if (Breaks.find().fetch()[0]) {
      breaks = Breaks.find().fetch()[0].breaks;
    } else {
      breaks = "";
    }

    return {
      datetimeLocal: datetimeLocal,
      datetime: datetime,
      wh: workedHours,
      hr: hourlyRate,
      start: startTime,
      end: endTime,
      breaks: breaks,
      earnings: earningsData,
      total: total
    };

  },

  changeInput: function (e) {
    var newValue = e.target.value;
    var id = e.target.id;
    Meteor.call("earningsUpdate", id, newValue);
  },

  delete: function (node) {
    Meteor.call("earningsRemove", node);
  },

  render: function () {
    return (
      <div className='center'>

        <div>
          <ExistingEarningsContainer />

          <WHxHR_Container wh={this.data.wh} hr={this.data.hr} />

          <StartEndContainer
            datetimeLocal={this.data.datetimeLocal}
            datetime={this.data.datetime}
            start={this.data.start}
            datetime={this.data.datetime}
            end={this.data.end}
            hr={this.data.hr}
            breaks={this.data.breaks} />

        </div>

        <ul> {this.data.earnings} </ul>

        <div>
          <p className="total center">Total = {this.data.total}</p>
          <PayoutContainer />
        </div>

      </div>
    );
  }

});
