// if you create a new collection, you need to create it on server side too
WorkedHours = new Mongo.Collection("workedHours");
HourlyRate = new Mongo.Collection("hourlyRate");
StartTime = new Mongo.Collection("startTime");
EndTime = new Mongo.Collection("endTime");
Breaks = new Mongo.Collection("breaks");
Earnings = new Mongo.Collection("earnings");

Meteor.startup(function () {
  // Use Meteor.startup to render the component after the page is ready
  Tracker.autorun(function(){

    if (Meteor.user() ) {

      setTimeout(function(){
         ReactDOM.render(<App />, document.getElementById("render-target"));
      }, 5);

    }

  });

});
