// create collection in both client and server side code
WorkedHours = new Mongo.Collection("workedHours");
HourlyRate = new Mongo.Collection("hourlyRate");
StartTime = new Mongo.Collection("startTime");
EndTime = new Mongo.Collection("endTime");
Breaks = new Mongo.Collection("breaks");
Earnings = new Mongo.Collection("earnings");

Meteor.startup(function(){

  Meteor.publish("workedHours", function(){
    return WorkedHours.find({owner: this.userId});
  });

  Meteor.publish("hourlyRate", function(){
    return HourlyRate.find({owner: this.userId});
  });

  Meteor.publish("startTime", function(){
    return StartTime.find({owner: this.userId});
  });

  Meteor.publish("endTime", function(){
    return EndTime.find({owner: this.userId});
  });

  Meteor.publish("breaks", function(){
    return Breaks.find({owner: this.userId});
  });

  Meteor.publish("earnings", function(){
    return Earnings.find({owner: this.userId});
  });

});

Meteor.methods({

  earningsInsert: function(value){
    Earnings.insert({
      money: value,
      owner: Meteor.userId()
    });
  },

  workedHoursInsert: function(whValue) {
    WorkedHours.insert({
      workedHours: whValue,
      owner: Meteor.userId()
    });
  },

  hourlyRateInsert: function(hrValue) {
    HourlyRate.insert({
      hourlyRate: hrValue,
      owner: Meteor.userId()
    });
  },

  startTimeInsert: function(startTimeValue) {
    StartTime.insert({
      startTime: startTimeValue,
      owner: Meteor.userId()
    });
  },

  endTimeInsert: function(endTimeValue) {
    EndTime.insert({
      endTime: endTimeValue,
      owner: Meteor.userId()
    });
  },

  breaksInsert: function(breaks) {
    Breaks.insert({
      breaks: breaks,
      owner: Meteor.userId()
    });
  },

  earningsRemove: function(idsToRemove) {
    Earnings.remove( idsToRemove );
  },

  workedHoursUpdate: function(id, whValue) {
    WorkedHours.update({_id:id}, {$set:{
      workedHours: whValue
    }});
  },

  hourlyRateUpdate: function(id, hrValue) {
    HourlyRate.update({_id:id}, {$set:{
      hourlyRate: hrValue
    }});
  },

  startTimeUpdate: function(id, startTimeValue) {
    StartTime.update({_id:id}, {$set:{
      startTime: startTimeValue
    }});
  },

  endTimeUpdate: function(id, endTimeValue) {
    EndTime.update({_id:id}, {$set:{
      endTime: endTimeValue
    }});
  },

  breaksUpdate: function(id, breaks) {
    Breaks.update({_id:id}, {$set:{
      breaks: breaks
    }});
  },

  earningsUpdate: function(id, emv) {
    Earnings.update({ _id: id }, {$set:{
      money: emv
    }});
  }

});
